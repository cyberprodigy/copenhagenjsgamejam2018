"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Resource_1 = require("./src/Resource");
var Shelf_1 = require("./src/Shelf");
var ShelfItem_1 = require("./src/ShelfItem");
var IOCContainer_1 = require("./src/IOCContainer");
var App = /** @class */ (function () {
    function App() {
        this.game = new Phaser.Game(1024, 768, Phaser.AUTO, 'phaser-example', { preload: this.preload, create: this.create, update: this.update, render: this.render });
        IOCContainer_1.IOCContainer.game = this.game;
    }
    App.prototype.preload = function () {
        //this.game.load.image(Resource.SHELF.toString(), 'assets/sprites/shelf.jpg');
        this.game.load.image(Resource_1.Resource.TOY_CAR_1.toString(), 'assets/sprites/toy_car_1.png');
    };
    App.prototype.update = function () {
        // console.log("Prupdateeload");
    };
    App.prototype.render = function () {
        // console.log("render");
    };
    App.prototype.create = function () {
        console.log(this.game.stage.backgroundColor = "#FF8800");
        var shelf = new Shelf_1.Shelf();
        shelf.AddItem(new ShelfItem_1.ShelfItem("ToyCar", 25, Resource_1.Resource.TOY_CAR_1));
    };
    return App;
}());
exports.App = App;
window.onload = function () {
    var game = new App();
};
//# sourceMappingURL=App.js.map