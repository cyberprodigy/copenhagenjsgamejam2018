import {Resource} from "./src/Resource"
import { Shelf } from "./src/Shelf";
import { ShelfItem } from "./src/ShelfItem";
import { IOCContainer } from "./src/IOCContainer";


export class App {
    public game: Phaser.Game;

    constructor() {
        this.game = new Phaser.Game(1024, 768, Phaser.AUTO, 'phaser-example', { preload: this.preload, create: this.create, update: this.update, render: this.render });
        IOCContainer.game = this.game;
    }

    

    preload() {
        //this.game.load.image(Resource.SHELF.toString(), 'assets/sprites/shelf.jpg');
        this.game.load.image(Resource.TOY_CAR_1.toString(), 'assets/sprites/toy_car_1.png');
    }

    update() {
       // console.log("Prupdateeload");
        
    }

    render() {
       // console.log("render");
        
    }

    create() {
        console.log(this.game.stage.backgroundColor="#FF8800");
        var shelf:Shelf = new Shelf();
        shelf.AddItem(new ShelfItem("ToyCar", 25, Resource.TOY_CAR_1))
        
    }

}

window.onload = () => {
    var game = new App();
};

