"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Resource;
(function (Resource) {
    Resource[Resource["SHELF"] = 0] = "SHELF";
    Resource[Resource["TOY_CAR_1"] = 1] = "TOY_CAR_1";
    Resource[Resource["TOY_CAR_2"] = 2] = "TOY_CAR_2";
    Resource[Resource["TV_SET"] = 3] = "TV_SET";
    Resource[Resource["RED_VINE"] = 4] = "RED_VINE";
    Resource[Resource["CHOCOLATE_CANDY_BOXS"] = 5] = "CHOCOLATE_CANDY_BOXS";
})(Resource = exports.Resource || (exports.Resource = {}));
//# sourceMappingURL=Resource.js.map