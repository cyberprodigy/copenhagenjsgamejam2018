"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventDispatcher_1 = require("./EventDispatcher");
var Event_1 = require("./Event");
var ShelfItem = /** @class */ (function (_super) {
    __extends(ShelfItem, _super);
    function ShelfItem(name, price, graphicsResource) {
        var _this = _super.call(this) || this;
        _this.name = name;
        _this.price = price;
        _this.graphicsResource = graphicsResource;
        return _this;
    }
    ShelfItem.prototype.display = function () {
        this.Ui.events.onInputDown.add(this.onClick, this);
    };
    ShelfItem.prototype.Hide = function () {
        this.Ui.destroy();
    };
    ShelfItem.prototype.onClick = function () {
        this.dispatch(Event_1.Event.CLICKED);
    };
    return ShelfItem;
}(EventDispatcher_1.EventDispatcher));
exports.ShelfItem = ShelfItem;
//# sourceMappingURL=ShelfItem.js.map