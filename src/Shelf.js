"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Shelf = /** @class */ (function () {
    function Shelf() {
        this.mItems = [];
    }
    Shelf.prototype.AddItem = function (item) {
        this.mItems.push(item);
        item.addListener("clicked", this.onItemClicked);
        item.display();
        item.Ui.x = 50;
    };
    Shelf.prototype.RemoveItem = function (item) {
        var idx = this.mItems.indexOf(item);
        if (idx > -1) {
            item.Hide();
            this.mItems.splice(idx, 1);
            return true;
        }
        return false;
    };
    Shelf.prototype.onItemClicked = function (shelfItem) {
        this.RemoveItem(shelfItem);
        console.log("The item was clicked " + shelfItem.name);
    };
    return Shelf;
}());
exports.Shelf = Shelf;
//# sourceMappingURL=Shelf.js.map