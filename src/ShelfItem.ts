import { Resource } from "./Resource";
import { EventDispatcher } from "./EventDispatcher";
import { IOCContainer } from "./IOCContainer";
import { Event } from "./Event";

export class ShelfItem extends EventDispatcher
{
    
    public Ui:Phaser.Sprite;
    constructor(public name:string, public price:number, public graphicsResource:Resource )
    {
       super();

    }

    public display():void{
        this.Ui.events.onInputDown.add(this.onClick, this);
    }

    public Hide(): void {
        this.Ui.destroy();
    }

    public onClick():void
    {
        this.dispatch(Event.CLICKED);
    }


}