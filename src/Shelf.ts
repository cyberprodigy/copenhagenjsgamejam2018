import { ShelfItem } from "./ShelfItem";
import { Resource } from "./Resource";

export class Shelf
{
    private mItems:Array<ShelfItem> = [];
    constructor()
    {

    }

    public AddItem(item:ShelfItem)
    {
        this.mItems.push(item);
        item.addListener("clicked", this.onItemClicked);
        item.display();
        item.Ui.x = 50;
    }

    public RemoveItem(item:ShelfItem):boolean
    {
        var idx:number = this.mItems.indexOf(item);
        if(idx > -1)
        {
            item.Hide();
            this.mItems.splice(idx, 1);
            return true;
        }
        return false;
        
    }

    private onItemClicked(shelfItem:ShelfItem):void
    {
        this.RemoveItem(shelfItem);
        console.log("The item was clicked " + shelfItem.name);
    }
}