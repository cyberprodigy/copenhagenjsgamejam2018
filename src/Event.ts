export enum Event
{
    CLICKED,
    MESSAGE_RECEIVED
}