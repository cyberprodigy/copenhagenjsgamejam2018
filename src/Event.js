"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Event;
(function (Event) {
    Event[Event["CLICKED"] = 0] = "CLICKED";
    Event[Event["MESSAGE_RECEIVED"] = 1] = "MESSAGE_RECEIVED";
})(Event = exports.Event || (exports.Event = {}));
//# sourceMappingURL=Event.js.map