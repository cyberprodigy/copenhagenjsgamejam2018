import { Event } from "./Event";

/**
 * Simple (Event)Dispatcher class
 * Inspiered by https://medium.com/@LeoAref/simple-event-dispatcher-implementation-using-javascript-36d0eadf5a11
 */
export class EventDispatcher {
    events: any;
  
    constructor() {
      this.events = {};
    }
  
    public addListener(event: string, callback: (data?: any) => any):void {
      // Create the event if not exists
      if (this.events[event] === undefined) {
        this.events[event] = {
          listeners: []
        };
      }
      this.events[event].listeners.push(callback);
    }
  
    public removeListener(event: string, callback: (data?: any) => any):boolean {
      // Check if this event not exists
      if (this.events[event] === undefined) {
        return false;
      }
      this.events[event].listeners = this.events[event].listeners.filter(
        (listener: string) => {
          return listener.toString() !== callback.toString();
        }
      );
      return true;
    }
  
    public dispatch(event: Event, data?: any):boolean {
      // Check if this event not exists
      if (this.events[event] === undefined) {
        return false;
      }
      this.events[event].listeners.forEach((listener: any) => {
        listener(data);
      });
      return true;
    }
  }